﻿
Public Class Form1
    Dim download_size As Long
    Dim downloaded_size As Long
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles Download.DoWork
        My.Computer.Network.DownloadFile("http://vlauncher.cba.pl/Vlauncher.exe", My.Application.Info.DirectoryPath & "\Vlauncher.exe", "", "", False, 360000, True)
    End Sub

    Private Sub Progress_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Progress.Tick
        downloaded_size = My.Computer.FileSystem.GetFileInfo(My.Application.Info.DirectoryPath & "\Vlauncher.exe").Length
        FlatProgressBar1.Value = downloaded_size
        If FlatProgressBar1.Value = FlatProgressBar1.Maximum Then
            FlatButton2.Enabled = True
            FlatButton2.BackColor = Color.LightCoral
        End If

    End Sub
    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) Handles FlatButton1.Click

        If My.Computer.Network.Ping("74.125.224.72") Then
            If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\Vlauncher.exe") Then
                Dim req As System.Net.WebRequest
                Dim resp As System.Net.WebResponse
                req = Net.WebRequest.Create("http://vlauncher.cba.pl/Vlauncher.exe")
                resp = req.GetResponse
                req.Method = Net.WebRequestMethods.Http.Get
                download_size = resp.ContentLength
                FlatProgressBar1.Maximum = download_size
                Download.RunWorkerAsync()
                Progress.Start()
            Else
                MsgBox("Nie wykryto Vlaunchera!")
            End If
        Else
            MsgBox("Wystąpił błąd podczas łączenia z serwerem")
            Me.Close()
        End If
    End Sub

    Private Sub FlatButton2_Click(sender As Object, e As EventArgs) Handles FlatButton2.Click
        Dim miejsce As String = My.Application.Info.DirectoryPath & "\Vlauncher.exe"
        Process.Start(miejsce)
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("http://veterani.pl/")
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        System.Diagnostics.Process.Start("ts3server://178.217.189.111?port=9987")
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Process.Start("https://plus.google.com/u/0/108935871591331874521/posts")
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Process.Start("https://pl-pl.facebook.com/pages/veteranipl/145842635427485")
    End Sub

    Private Sub PictureBox4_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox4.MouseEnter
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox4_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox4.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub PictureBox6_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub PictureBox7_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub PictureBox3_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox3.MouseEnter
        Me.Cursor = Cursors.Hand
    End Sub
    Private Sub Label3_MouseLeave(sender As Object, e As EventArgs) Handles Label3.MouseLeave
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Label1.Text = "Poczekaj na zaktualizowanie się Vlaunchera."
        Label2.Text = "W razie problemów zapraszamy na"
        Label3.Text = "lub"
        LinkLabel1.Text = "Naszą stronę"
        LinkLabel2.Text = "Naszego TS'a."
        FlatButton2.Text = "Gotowe"

    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Label1.Text = "Please wait for Vlauncher update"
        Label2.Text = "                   If you have any problem,"
        Label3.Text = "and"
        LinkLabel1.Text = "here's our website"
        LinkLabel2.Text = "TeamSpeak."
        FlatButton2.Text = "Ready"
    End Sub

    Private Sub PictureBox_Click(sender As Object, e As EventArgs) Handles PictureBox.Click
        Process.Start("https://www.youtube.com/user/Veteranipl/feed")
    End Sub

    Private Sub PictureBox_MouseEnter(sender As Object, e As EventArgs) Handles PictureBox.MouseEnter
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        FlatButton2.Enabled = False
        FlatButton2.BackColor = Color.Maroon
    End Sub
End Class
