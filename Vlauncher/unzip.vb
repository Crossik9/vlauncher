﻿Imports System.Environment
Imports Ionic.Zip
Module SimpleUnzip
    Public Sub Unzip(ByVal ZipToUnpack As String, ByVal DirectoryToExstractTo As String)

        Try
            Using zip As ZipFile = ZipFile.Read(ZipToUnpack)
                Form1.ProgressBar2.Maximum = zip.Entries.Count
                Dim entry As ZipEntry
                For Each entry In zip
                    entry.Extract(DirectoryToExstractTo, ExtractExistingFileAction.OverwriteSilently)
                    Form1.ProgressBar2.Value += 1
                    Form1.ProgressBar2.Refresh()
                    Dim progres As String = Form1.ProgressBar2.Value.ToString / zip.Entries.Count.ToString
                    If Form1.FlatLabel15.Text = "99 %" Then
                    Else
                        Form1.FlatLabel15.Text = progres.Substring(0, 4).Remove(0, 2) + " %"
                        Form1.FlatLabel15.Refresh()
                    End If
                    System.Threading.Thread.Sleep(50)
                    Form1.Cursor = Cursors.AppStarting
                Next
                Form1.FlatLabel15.Text = "Gotowe"
                Form1.ProgressBar2.Value = 0
                Form1.Cursor = Cursors.Default
                Beep()
            End Using
        Catch ex1 As Exception
            MsgBox("Exception: " & ex1.ToString())
        End Try
    End Sub
End Module