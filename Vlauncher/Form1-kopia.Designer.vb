﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.FormSkin1 = New Vlauncher.FormSkin()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.FlatLabel19 = New Vlauncher.FlatLabel()
        Me.autostartToggleButton = New Vlauncher.FlatToggle()
        Me.FlatLabel18 = New Vlauncher.FlatLabel()
        Me.FlatButton6 = New Vlauncher.FlatButton()
        Me.FlatColorPalette1 = New Vlauncher.FlatColorPalette()
        Me.FlatButton5 = New Vlauncher.FlatButton()
        Me.FlatComboBox1 = New Vlauncher.FlatComboBox()
        Me.FlatLabel17 = New Vlauncher.FlatLabel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.FlatTextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox10x = New System.Windows.Forms.TextBox()
        Me.FlatLabel16 = New Vlauncher.FlatLabel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.FlatButton2 = New Vlauncher.FlatButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CheckBox6x = New Vlauncher.FlatToggle()
        Me.checkbox9x = New Vlauncher.FlatToggle()
        Me.Checkbox3x = New Vlauncher.FlatToggle()
        Me.CheckBox4x = New Vlauncher.FlatToggle()
        Me.CheckBox1x = New Vlauncher.FlatToggle()
        Me.FlatLabel14 = New Vlauncher.FlatLabel()
        Me.FlatLabel13 = New Vlauncher.FlatLabel()
        Me.FlatLabel12 = New Vlauncher.FlatLabel()
        Me.FlatLabel11 = New Vlauncher.FlatLabel()
        Me.FlatLabel10 = New Vlauncher.FlatLabel()
        Me.FlatLabel9 = New Vlauncher.FlatLabel()
        Me.checkbox5x = New Vlauncher.FlatToggle()
        Me.checkbox7x = New Vlauncher.FlatToggle()
        Me.checkbox10x = New Vlauncher.FlatToggle()
        Me.CheckBox8x = New Vlauncher.FlatToggle()
        Me.Checkbox12x = New Vlauncher.FlatToggle()
        Me.CheckBox11x = New Vlauncher.FlatToggle()
        Me.FlatLabel8 = New Vlauncher.FlatLabel()
        Me.FlatLabel7 = New Vlauncher.FlatLabel()
        Me.FlatLabel6 = New Vlauncher.FlatLabel()
        Me.FlatLabel5 = New Vlauncher.FlatLabel()
        Me.FlatLabel4 = New Vlauncher.FlatLabel()
        Me.FlatLabel2 = New Vlauncher.FlatLabel()
        Me.Checkbox13x = New Vlauncher.FlatToggle()
        Me.FlatLabel1 = New Vlauncher.FlatLabel()
        Me.checkbox14x = New Vlauncher.FlatToggle()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ComboBox3x = New Vlauncher.FlatComboBox()
        Me.ComboBox4 = New Vlauncher.FlatComboBox()
        Me.ComboBox1x = New Vlauncher.FlatComboBox()
        Me.ComboBox2x = New Vlauncher.FlatComboBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.FlatAlertBox1 = New Vlauncher.FlatAlertBox()
        Me.PictureBox = New System.Windows.Forms.PictureBox()
        Me.FlatTabControl1 = New Vlauncher.FlatTabControl()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.FlatLabel15 = New Vlauncher.FlatLabel()
        Me.ProgressBar2 = New Vlauncher.FlatProgressBar()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.FlatButton3 = New Vlauncher.FlatButton()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.FlatButton1 = New Vlauncher.FlatButton()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.TabPage2x = New System.Windows.Forms.TabPage()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.FlatButton4 = New Vlauncher.FlatButton()
        Me.FlatTextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox5x = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.modyx = New System.Windows.Forms.TextBox()
        Me.TextBox2x = New System.Windows.Forms.TextBox()
        Me.Button7x = New Vlauncher.FlatButton()
        Me.Button4x = New Vlauncher.FlatButton()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Button9x = New Vlauncher.FlatButton()
        Me.FlatLabel3 = New Vlauncher.FlatLabel()
        Me.FlatMini1 = New Vlauncher.FlatMini()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.FlatClose1 = New Vlauncher.FlatClose()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.FormSkin1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlatTabControl1.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2x.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Timer2
        '
        Me.Timer2.Enabled = True
        Me.Timer2.Interval = 1000
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "*.zip|"
        Me.OpenFileDialog1.RestoreDirectory = True
        '
        'Timer3
        '
        Me.Timer3.Interval = 5000
        '
        'FormSkin1
        '
        Me.FormSkin1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FormSkin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.FormSkin1.Controls.Add(Me.Panel2)
        Me.FormSkin1.Controls.Add(Me.PictureBox10)
        Me.FormSkin1.Controls.Add(Me.FlatAlertBox1)
        Me.FormSkin1.Controls.Add(Me.PictureBox)
        Me.FormSkin1.Controls.Add(Me.FlatTabControl1)
        Me.FormSkin1.Controls.Add(Me.FlatMini1)
        Me.FormSkin1.Controls.Add(Me.PictureBox2)
        Me.FormSkin1.Controls.Add(Me.FlatClose1)
        Me.FormSkin1.Controls.Add(Me.PictureBox3)
        Me.FormSkin1.Controls.Add(Me.PictureBox4)
        Me.FormSkin1.Controls.Add(Me.Panel1)
        Me.FormSkin1.Controls.Add(Me.PictureBox9)
        Me.FormSkin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormSkin1.FlatColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FormSkin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.HeaderMaximize = False
        Me.FormSkin1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FormSkin1.Location = New System.Drawing.Point(0, 0)
        Me.FormSkin1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FormSkin1.Name = "FormSkin1"
        Me.FormSkin1.Size = New System.Drawing.Size(1067, 455)
        Me.FormSkin1.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.Panel2.Controls.Add(Me.FlatLabel19)
        Me.Panel2.Controls.Add(Me.autostartToggleButton)
        Me.Panel2.Controls.Add(Me.FlatLabel18)
        Me.Panel2.Controls.Add(Me.FlatButton6)
        Me.Panel2.Controls.Add(Me.FlatColorPalette1)
        Me.Panel2.Controls.Add(Me.FlatButton5)
        Me.Panel2.Controls.Add(Me.FlatComboBox1)
        Me.Panel2.Controls.Add(Me.FlatLabel17)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.FlatTextBox2)
        Me.Panel2.Controls.Add(Me.TextBox10x)
        Me.Panel2.Controls.Add(Me.FlatLabel16)
        Me.Panel2.Controls.Add(Me.PictureBox7)
        Me.Panel2.Controls.Add(Me.PictureBox6)
        Me.Panel2.Controls.Add(Me.FlatButton2)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.CheckBox6x)
        Me.Panel2.Controls.Add(Me.checkbox9x)
        Me.Panel2.Controls.Add(Me.Checkbox3x)
        Me.Panel2.Controls.Add(Me.CheckBox4x)
        Me.Panel2.Controls.Add(Me.CheckBox1x)
        Me.Panel2.Controls.Add(Me.FlatLabel14)
        Me.Panel2.Controls.Add(Me.FlatLabel13)
        Me.Panel2.Controls.Add(Me.FlatLabel12)
        Me.Panel2.Controls.Add(Me.FlatLabel11)
        Me.Panel2.Controls.Add(Me.FlatLabel10)
        Me.Panel2.Controls.Add(Me.FlatLabel9)
        Me.Panel2.Controls.Add(Me.checkbox5x)
        Me.Panel2.Controls.Add(Me.checkbox7x)
        Me.Panel2.Controls.Add(Me.checkbox10x)
        Me.Panel2.Controls.Add(Me.CheckBox8x)
        Me.Panel2.Controls.Add(Me.Checkbox12x)
        Me.Panel2.Controls.Add(Me.CheckBox11x)
        Me.Panel2.Controls.Add(Me.FlatLabel8)
        Me.Panel2.Controls.Add(Me.FlatLabel7)
        Me.Panel2.Controls.Add(Me.FlatLabel6)
        Me.Panel2.Controls.Add(Me.FlatLabel5)
        Me.Panel2.Controls.Add(Me.FlatLabel4)
        Me.Panel2.Controls.Add(Me.FlatLabel2)
        Me.Panel2.Controls.Add(Me.Checkbox13x)
        Me.Panel2.Controls.Add(Me.FlatLabel1)
        Me.Panel2.Controls.Add(Me.checkbox14x)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.ComboBox3x)
        Me.Panel2.Controls.Add(Me.ComboBox4)
        Me.Panel2.Controls.Add(Me.ComboBox1x)
        Me.Panel2.Controls.Add(Me.ComboBox2x)
        Me.Panel2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Panel2.Location = New System.Drawing.Point(981, 36)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(86, 38)
        Me.Panel2.TabIndex = 79
        Me.Panel2.Visible = False
        '
        'FlatLabel19
        '
        Me.FlatLabel19.AutoSize = True
        Me.FlatLabel19.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel19.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel19.ForeColor = System.Drawing.Color.White
        Me.FlatLabel19.Location = New System.Drawing.Point(294, 316)
        Me.FlatLabel19.Name = "FlatLabel19"
        Me.FlatLabel19.Size = New System.Drawing.Size(71, 19)
        Me.FlatLabel19.TabIndex = 151
        Me.FlatLabel19.Text = "Autostart"
        Me.FlatLabel19.Visible = False
        '
        'autostartToggleButton
        '
        Me.autostartToggleButton.BackColor = System.Drawing.Color.Transparent
        Me.autostartToggleButton.Checked = False
        Me.autostartToggleButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.autostartToggleButton.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.autostartToggleButton.Location = New System.Drawing.Point(447, 305)
        Me.autostartToggleButton.Name = "autostartToggleButton"
        Me.autostartToggleButton.Options = Vlauncher.FlatToggle._Options.Style3
        Me.autostartToggleButton.Size = New System.Drawing.Size(76, 33)
        Me.autostartToggleButton.TabIndex = 150
        Me.autostartToggleButton.Text = "FlatToggle1"
        Me.autostartToggleButton.Visible = False
        '
        'FlatLabel18
        '
        Me.FlatLabel18.AutoSize = True
        Me.FlatLabel18.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel18.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel18.ForeColor = System.Drawing.Color.White
        Me.FlatLabel18.Location = New System.Drawing.Point(294, 364)
        Me.FlatLabel18.Name = "FlatLabel18"
        Me.FlatLabel18.Size = New System.Drawing.Size(46, 19)
        Me.FlatLabel18.TabIndex = 149
        Me.FlatLabel18.Text = "Profil"
        '
        'FlatButton6
        '
        Me.FlatButton6.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton6.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatButton6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton6.Font = New System.Drawing.Font("Segoe UI Black", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatButton6.Location = New System.Drawing.Point(574, 364)
        Me.FlatButton6.Name = "FlatButton6"
        Me.FlatButton6.Rounded = False
        Me.FlatButton6.Size = New System.Drawing.Size(19, 25)
        Me.FlatButton6.TabIndex = 148
        Me.FlatButton6.Text = "x"
        Me.FlatButton6.TextColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        '
        'FlatColorPalette1
        '
        Me.FlatColorPalette1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatColorPalette1.Black = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatColorPalette1.Blue = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.FlatColorPalette1.Cyan = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(154, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.FlatColorPalette1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatColorPalette1.Gray = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatColorPalette1.LimeGreen = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatColorPalette1.Location = New System.Drawing.Point(887, 66)
        Me.FlatColorPalette1.Name = "FlatColorPalette1"
        Me.FlatColorPalette1.Orange = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(181, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.FlatColorPalette1.Purple = System.Drawing.Color.FromArgb(CType(CType(155, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(181, Byte), Integer))
        Me.FlatColorPalette1.Red = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatColorPalette1.Size = New System.Drawing.Size(180, 80)
        Me.FlatColorPalette1.TabIndex = 147
        Me.FlatColorPalette1.Text = "FlatColorPalette1"
        Me.FlatColorPalette1.Visible = False
        Me.FlatColorPalette1.White = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatButton5
        '
        Me.FlatButton5.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton5.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatButton5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton5.Font = New System.Drawing.Font("Segoe UI Black", 10.2!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatButton5.Location = New System.Drawing.Point(588, 364)
        Me.FlatButton5.Name = "FlatButton5"
        Me.FlatButton5.Rounded = False
        Me.FlatButton5.Size = New System.Drawing.Size(19, 25)
        Me.FlatButton5.TabIndex = 146
        Me.FlatButton5.Text = "+"
        Me.FlatButton5.TextColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        '
        'FlatComboBox1
        '
        Me.FlatComboBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.FlatComboBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.FlatComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FlatComboBox1.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatComboBox1.ForeColor = System.Drawing.Color.White
        Me.FlatComboBox1.FormattingEnabled = True
        Me.FlatComboBox1.HoverColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatComboBox1.ItemHeight = 18
        Me.FlatComboBox1.Location = New System.Drawing.Point(447, 364)
        Me.FlatComboBox1.Name = "FlatComboBox1"
        Me.FlatComboBox1.Size = New System.Drawing.Size(121, 24)
        Me.FlatComboBox1.TabIndex = 145
        '
        'FlatLabel17
        '
        Me.FlatLabel17.AutoSize = True
        Me.FlatLabel17.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel17.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel17.ForeColor = System.Drawing.Color.White
        Me.FlatLabel17.Location = New System.Drawing.Point(294, 172)
        Me.FlatLabel17.Name = "FlatLabel17"
        Me.FlatLabel17.Size = New System.Drawing.Size(127, 19)
        Me.FlatLabel17.TabIndex = 144
        Me.FlatLabel17.Text = "Priorytet procesu"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.Panel3.Location = New System.Drawing.Point(282, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(4, 520)
        Me.Panel3.TabIndex = 143
        '
        'FlatTextBox2
        '
        Me.FlatTextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.FlatTextBox2.ForeColor = System.Drawing.Color.Silver
        Me.FlatTextBox2.Location = New System.Drawing.Point(447, 17)
        Me.FlatTextBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatTextBox2.Multiline = True
        Me.FlatTextBox2.Name = "FlatTextBox2"
        Me.FlatTextBox2.Size = New System.Drawing.Size(321, 33)
        Me.FlatTextBox2.TabIndex = 142
        Me.FlatTextBox2.Tag = ""
        '
        'TextBox10x
        '
        Me.TextBox10x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox10x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox10x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox10x.Location = New System.Drawing.Point(555, 68)
        Me.TextBox10x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox10x.Multiline = True
        Me.TextBox10x.Name = "TextBox10x"
        Me.TextBox10x.Size = New System.Drawing.Size(213, 33)
        Me.TextBox10x.TabIndex = 141
        Me.TextBox10x.Tag = ""
        '
        'FlatLabel16
        '
        Me.FlatLabel16.AutoSize = True
        Me.FlatLabel16.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel16.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel16.ForeColor = System.Drawing.Color.White
        Me.FlatLabel16.Location = New System.Drawing.Point(294, 28)
        Me.FlatLabel16.Name = "FlatLabel16"
        Me.FlatLabel16.Size = New System.Drawing.Size(120, 19)
        Me.FlatLabel16.TabIndex = 140
        Me.FlatLabel16.Text = "Ścieżka do army"
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(1002, 3)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(33, 22)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 138
        Me.PictureBox7.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(963, 3)
        Me.PictureBox6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(33, 23)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 137
        Me.PictureBox6.TabStop = False
        '
        'FlatButton2
        '
        Me.FlatButton2.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton2.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton2.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton2.Location = New System.Drawing.Point(843, 380)
        Me.FlatButton2.Name = "FlatButton2"
        Me.FlatButton2.Rounded = False
        Me.FlatButton2.Size = New System.Drawing.Size(105, 27)
        Me.FlatButton2.TabIndex = 133
        Me.FlatButton2.Text = "Aktualizuj"
        Me.FlatButton2.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(990, 377)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 28)
        Me.Label2.TabIndex = 134
        Me.Label2.Text = "XXX"
        '
        'CheckBox6x
        '
        Me.CheckBox6x.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox6x.Checked = False
        Me.CheckBox6x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox6x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.CheckBox6x.Location = New System.Drawing.Point(174, 161)
        Me.CheckBox6x.Name = "CheckBox6x"
        Me.CheckBox6x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.CheckBox6x.Size = New System.Drawing.Size(76, 33)
        Me.CheckBox6x.TabIndex = 132
        Me.CheckBox6x.Text = "FlatToggle2"
        '
        'checkbox9x
        '
        Me.checkbox9x.BackColor = System.Drawing.Color.Transparent
        Me.checkbox9x.Checked = False
        Me.checkbox9x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkbox9x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.checkbox9x.Location = New System.Drawing.Point(174, 17)
        Me.checkbox9x.Name = "checkbox9x"
        Me.checkbox9x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.checkbox9x.Size = New System.Drawing.Size(76, 33)
        Me.checkbox9x.TabIndex = 131
        Me.checkbox9x.Text = "FlatToggle6"
        '
        'Checkbox3x
        '
        Me.Checkbox3x.BackColor = System.Drawing.Color.Transparent
        Me.Checkbox3x.Checked = False
        Me.Checkbox3x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Checkbox3x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.Checkbox3x.Location = New System.Drawing.Point(447, 161)
        Me.Checkbox3x.Name = "Checkbox3x"
        Me.Checkbox3x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.Checkbox3x.Size = New System.Drawing.Size(76, 33)
        Me.Checkbox3x.TabIndex = 130
        Me.Checkbox3x.Text = "FlatToggle5"
        '
        'CheckBox4x
        '
        Me.CheckBox4x.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox4x.Checked = False
        Me.CheckBox4x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox4x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.CheckBox4x.Location = New System.Drawing.Point(447, 209)
        Me.CheckBox4x.Name = "CheckBox4x"
        Me.CheckBox4x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.CheckBox4x.Size = New System.Drawing.Size(76, 33)
        Me.CheckBox4x.TabIndex = 129
        Me.CheckBox4x.Text = "FlatToggle4"
        '
        'CheckBox1x
        '
        Me.CheckBox1x.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1x.Checked = False
        Me.CheckBox1x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox1x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.CheckBox1x.Location = New System.Drawing.Point(174, 209)
        Me.CheckBox1x.Name = "CheckBox1x"
        Me.CheckBox1x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.CheckBox1x.Size = New System.Drawing.Size(76, 33)
        Me.CheckBox1x.TabIndex = 128
        Me.CheckBox1x.Text = "FlatToggle3"
        '
        'FlatLabel14
        '
        Me.FlatLabel14.AutoSize = True
        Me.FlatLabel14.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel14.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel14.ForeColor = System.Drawing.Color.White
        Me.FlatLabel14.Location = New System.Drawing.Point(7, 172)
        Me.FlatLabel14.Name = "FlatLabel14"
        Me.FlatLabel14.Size = New System.Drawing.Size(87, 19)
        Me.FlatLabel14.TabIndex = 127
        Me.FlatLabel14.Text = "Pomiń Intra"
        '
        'FlatLabel13
        '
        Me.FlatLabel13.AutoSize = True
        Me.FlatLabel13.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel13.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel13.ForeColor = System.Drawing.Color.White
        Me.FlatLabel13.Location = New System.Drawing.Point(294, 220)
        Me.FlatLabel13.Name = "FlatLabel13"
        Me.FlatLabel13.Size = New System.Drawing.Size(88, 19)
        Me.FlatLabel13.TabIndex = 126
        Me.FlatLabel13.Text = "Ilość Rdzeni"
        '
        'FlatLabel12
        '
        Me.FlatLabel12.AutoSize = True
        Me.FlatLabel12.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel12.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel12.ForeColor = System.Drawing.Color.White
        Me.FlatLabel12.Location = New System.Drawing.Point(294, 28)
        Me.FlatLabel12.Name = "FlatLabel12"
        Me.FlatLabel12.Size = New System.Drawing.Size(127, 19)
        Me.FlatLabel12.TabIndex = 125
        Me.FlatLabel12.Text = "Priorytet Procesu"
        '
        'FlatLabel11
        '
        Me.FlatLabel11.AutoSize = True
        Me.FlatLabel11.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel11.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel11.ForeColor = System.Drawing.Color.White
        Me.FlatLabel11.Location = New System.Drawing.Point(294, 124)
        Me.FlatLabel11.Name = "FlatLabel11"
        Me.FlatLabel11.Size = New System.Drawing.Size(146, 19)
        Me.FlatLabel11.TabIndex = 124
        Me.FlatLabel11.Text = "Maksymalna Pamięć"
        '
        'FlatLabel10
        '
        Me.FlatLabel10.AutoSize = True
        Me.FlatLabel10.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel10.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel10.ForeColor = System.Drawing.Color.White
        Me.FlatLabel10.Location = New System.Drawing.Point(7, 220)
        Me.FlatLabel10.Name = "FlatLabel10"
        Me.FlatLabel10.Size = New System.Drawing.Size(53, 19)
        Me.FlatLabel10.TabIndex = 123
        Me.FlatLabel10.Text = "WinXp"
        '
        'FlatLabel9
        '
        Me.FlatLabel9.AutoSize = True
        Me.FlatLabel9.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel9.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel9.ForeColor = System.Drawing.Color.White
        Me.FlatLabel9.Location = New System.Drawing.Point(7, 268)
        Me.FlatLabel9.Name = "FlatLabel9"
        Me.FlatLabel9.Size = New System.Drawing.Size(77, 19)
        Me.FlatLabel9.TabIndex = 122
        Me.FlatLabel9.Text = "Bez Pauzy"
        '
        'checkbox5x
        '
        Me.checkbox5x.BackColor = System.Drawing.Color.Transparent
        Me.checkbox5x.Checked = False
        Me.checkbox5x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkbox5x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.checkbox5x.Location = New System.Drawing.Point(174, 113)
        Me.checkbox5x.Name = "checkbox5x"
        Me.checkbox5x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.checkbox5x.Size = New System.Drawing.Size(76, 33)
        Me.checkbox5x.TabIndex = 121
        Me.checkbox5x.Text = "FlatToggle1"
        '
        'checkbox7x
        '
        Me.checkbox7x.BackColor = System.Drawing.Color.Transparent
        Me.checkbox7x.Checked = False
        Me.checkbox7x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkbox7x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.checkbox7x.Location = New System.Drawing.Point(174, 65)
        Me.checkbox7x.Name = "checkbox7x"
        Me.checkbox7x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.checkbox7x.Size = New System.Drawing.Size(76, 33)
        Me.checkbox7x.TabIndex = 120
        Me.checkbox7x.Text = "FlatToggle1"
        '
        'checkbox10x
        '
        Me.checkbox10x.BackColor = System.Drawing.Color.Transparent
        Me.checkbox10x.Checked = False
        Me.checkbox10x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkbox10x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.checkbox10x.Location = New System.Drawing.Point(447, 113)
        Me.checkbox10x.Name = "checkbox10x"
        Me.checkbox10x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.checkbox10x.Size = New System.Drawing.Size(76, 33)
        Me.checkbox10x.TabIndex = 119
        Me.checkbox10x.Text = "FlatToggle1"
        '
        'CheckBox8x
        '
        Me.CheckBox8x.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox8x.Checked = False
        Me.CheckBox8x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox8x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.CheckBox8x.Location = New System.Drawing.Point(174, 257)
        Me.CheckBox8x.Name = "CheckBox8x"
        Me.CheckBox8x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.CheckBox8x.Size = New System.Drawing.Size(76, 33)
        Me.CheckBox8x.TabIndex = 118
        Me.CheckBox8x.Text = "FlatToggle1"
        '
        'Checkbox12x
        '
        Me.Checkbox12x.BackColor = System.Drawing.Color.Transparent
        Me.Checkbox12x.Checked = False
        Me.Checkbox12x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Checkbox12x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.Checkbox12x.Location = New System.Drawing.Point(447, 65)
        Me.Checkbox12x.Name = "Checkbox12x"
        Me.Checkbox12x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.Checkbox12x.Size = New System.Drawing.Size(76, 33)
        Me.Checkbox12x.TabIndex = 117
        Me.Checkbox12x.Text = "FlatToggle1"
        '
        'CheckBox11x
        '
        Me.CheckBox11x.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox11x.Checked = False
        Me.CheckBox11x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CheckBox11x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.CheckBox11x.Location = New System.Drawing.Point(447, 257)
        Me.CheckBox11x.Name = "CheckBox11x"
        Me.CheckBox11x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.CheckBox11x.Size = New System.Drawing.Size(76, 33)
        Me.CheckBox11x.TabIndex = 116
        Me.CheckBox11x.Text = "FlatToggle1"
        '
        'FlatLabel8
        '
        Me.FlatLabel8.AutoSize = True
        Me.FlatLabel8.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel8.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel8.ForeColor = System.Drawing.Color.White
        Me.FlatLabel8.Location = New System.Drawing.Point(294, 268)
        Me.FlatLabel8.Name = "FlatLabel8"
        Me.FlatLabel8.Size = New System.Drawing.Size(79, 19)
        Me.FlatLabel8.TabIndex = 115
        Me.FlatLabel8.Text = "Po starcie "
        '
        'FlatLabel7
        '
        Me.FlatLabel7.AutoSize = True
        Me.FlatLabel7.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel7.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel7.ForeColor = System.Drawing.Color.White
        Me.FlatLabel7.Location = New System.Drawing.Point(294, 76)
        Me.FlatLabel7.Name = "FlatLabel7"
        Me.FlatLabel7.Size = New System.Drawing.Size(86, 19)
        Me.FlatLabel7.TabIndex = 114
        Me.FlatLabel7.Text = "Dodatkowe"
        '
        'FlatLabel6
        '
        Me.FlatLabel6.AutoSize = True
        Me.FlatLabel6.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel6.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel6.ForeColor = System.Drawing.Color.White
        Me.FlatLabel6.Location = New System.Drawing.Point(7, 124)
        Me.FlatLabel6.Name = "FlatLabel6"
        Me.FlatLabel6.Size = New System.Drawing.Size(108, 19)
        Me.FlatLabel6.TabIndex = 113
        Me.FlatLabel6.Text = "Tryb okienkwy"
        '
        'FlatLabel5
        '
        Me.FlatLabel5.AutoSize = True
        Me.FlatLabel5.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel5.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.FlatLabel5.ForeColor = System.Drawing.Color.White
        Me.FlatLabel5.Location = New System.Drawing.Point(7, 76)
        Me.FlatLabel5.Name = "FlatLabel5"
        Me.FlatLabel5.Size = New System.Drawing.Size(147, 19)
        Me.FlatLabel5.TabIndex = 112
        Me.FlatLabel5.Text = "Pokaż błędy skryptu"
        '
        'FlatLabel4
        '
        Me.FlatLabel4.AutoSize = True
        Me.FlatLabel4.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel4.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.FlatLabel4.ForeColor = System.Drawing.Color.White
        Me.FlatLabel4.Location = New System.Drawing.Point(7, 28)
        Me.FlatLabel4.Name = "FlatLabel4"
        Me.FlatLabel4.Size = New System.Drawing.Size(157, 19)
        Me.FlatLabel4.TabIndex = 111
        Me.FlatLabel4.Text = "Tylko spakowane pliki"
        '
        'FlatLabel2
        '
        Me.FlatLabel2.AutoSize = True
        Me.FlatLabel2.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel2.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel2.ForeColor = System.Drawing.Color.White
        Me.FlatLabel2.Location = New System.Drawing.Point(7, 364)
        Me.FlatLabel2.Name = "FlatLabel2"
        Me.FlatLabel2.Size = New System.Drawing.Size(162, 19)
        Me.FlatLabel2.TabIndex = 110
        Me.FlatLabel2.Text = "Sprawdzaj aktualizacje"
        '
        'Checkbox13x
        '
        Me.Checkbox13x.BackColor = System.Drawing.Color.Transparent
        Me.Checkbox13x.Checked = False
        Me.Checkbox13x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Checkbox13x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.Checkbox13x.Location = New System.Drawing.Point(174, 353)
        Me.Checkbox13x.Name = "Checkbox13x"
        Me.Checkbox13x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.Checkbox13x.Size = New System.Drawing.Size(76, 33)
        Me.Checkbox13x.TabIndex = 109
        Me.Checkbox13x.Text = "FlatToggle1"
        '
        'FlatLabel1
        '
        Me.FlatLabel1.AutoSize = True
        Me.FlatLabel1.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel1.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel1.ForeColor = System.Drawing.Color.White
        Me.FlatLabel1.Location = New System.Drawing.Point(7, 316)
        Me.FlatLabel1.Name = "FlatLabel1"
        Me.FlatLabel1.Size = New System.Drawing.Size(113, 19)
        Me.FlatLabel1.TabIndex = 108
        Me.FlatLabel1.Text = "Powiadomienia"
        '
        'checkbox14x
        '
        Me.checkbox14x.BackColor = System.Drawing.Color.Transparent
        Me.checkbox14x.Checked = False
        Me.checkbox14x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.checkbox14x.Font = New System.Drawing.Font("Segoe UI", 7.8!, System.Drawing.FontStyle.Bold)
        Me.checkbox14x.Location = New System.Drawing.Point(174, 305)
        Me.checkbox14x.Name = "checkbox14x"
        Me.checkbox14x.Options = Vlauncher.FlatToggle._Options.Style3
        Me.checkbox14x.Size = New System.Drawing.Size(76, 33)
        Me.checkbox14x.TabIndex = 107
        Me.checkbox14x.Text = "FlatToggle1"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1041, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 18)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 101
        Me.PictureBox1.TabStop = False
        '
        'ComboBox3x
        '
        Me.ComboBox3x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ComboBox3x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ComboBox3x.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBox3x.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3x.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.ComboBox3x.ForeColor = System.Drawing.Color.White
        Me.ComboBox3x.FormattingEnabled = True
        Me.ComboBox3x.HoverColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ComboBox3x.ItemHeight = 18
        Me.ComboBox3x.Items.AddRange(New Object() {"2047", "768", "1024"})
        Me.ComboBox3x.Location = New System.Drawing.Point(555, 116)
        Me.ComboBox3x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ComboBox3x.Name = "ComboBox3x"
        Me.ComboBox3x.Size = New System.Drawing.Size(121, 24)
        Me.ComboBox3x.TabIndex = 87
        '
        'ComboBox4
        '
        Me.ComboBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ComboBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ComboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.ComboBox4.ForeColor = System.Drawing.Color.White
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.HoverColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.ComboBox4.ItemHeight = 18
        Me.ComboBox4.Items.AddRange(New Object() {"Zamknij", "Zminimalizuj"})
        Me.ComboBox4.Location = New System.Drawing.Point(555, 260)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(121, 24)
        Me.ComboBox4.TabIndex = 92
        '
        'ComboBox1x
        '
        Me.ComboBox1x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ComboBox1x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ComboBox1x.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBox1x.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1x.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.ComboBox1x.ForeColor = System.Drawing.Color.White
        Me.ComboBox1x.FormattingEnabled = True
        Me.ComboBox1x.HoverColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ComboBox1x.ItemHeight = 18
        Me.ComboBox1x.Items.AddRange(New Object() {"Niski", "Średni", "Wysoki"})
        Me.ComboBox1x.Location = New System.Drawing.Point(555, 164)
        Me.ComboBox1x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ComboBox1x.Name = "ComboBox1x"
        Me.ComboBox1x.Size = New System.Drawing.Size(121, 24)
        Me.ComboBox1x.TabIndex = 89
        '
        'ComboBox2x
        '
        Me.ComboBox2x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ComboBox2x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ComboBox2x.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBox2x.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2x.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.ComboBox2x.ForeColor = System.Drawing.Color.White
        Me.ComboBox2x.FormattingEnabled = True
        Me.ComboBox2x.HoverColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ComboBox2x.ItemHeight = 18
        Me.ComboBox2x.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8"})
        Me.ComboBox2x.Location = New System.Drawing.Point(555, 212)
        Me.ComboBox2x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ComboBox2x.Name = "ComboBox2x"
        Me.ComboBox2x.Size = New System.Drawing.Size(121, 24)
        Me.ComboBox2x.TabIndex = 88
        '
        'PictureBox10
        '
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(981, 2)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(33, 27)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox10.TabIndex = 100
        Me.PictureBox10.TabStop = False
        '
        'FlatAlertBox1
        '
        Me.FlatAlertBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatAlertBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatAlertBox1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatAlertBox1.kind = Vlauncher.FlatAlertBox._Kind.Success
        Me.FlatAlertBox1.Location = New System.Drawing.Point(3, 449)
        Me.FlatAlertBox1.Name = "FlatAlertBox1"
        Me.FlatAlertBox1.Size = New System.Drawing.Size(1068, 42)
        Me.FlatAlertBox1.TabIndex = 22
        Me.FlatAlertBox1.Text = "FlatAlertBox1"
        Me.FlatAlertBox1.Visible = False
        '
        'PictureBox
        '
        Me.PictureBox.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox.Image = CType(resources.GetObject("PictureBox.Image"), System.Drawing.Image)
        Me.PictureBox.Location = New System.Drawing.Point(605, 4)
        Me.PictureBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Size = New System.Drawing.Size(29, 27)
        Me.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox.TabIndex = 30
        Me.PictureBox.TabStop = False
        '
        'FlatTabControl1
        '
        Me.FlatTabControl1.ActiveColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatTabControl1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatTabControl1.Controls.Add(Me.TabPage7)
        Me.FlatTabControl1.Controls.Add(Me.TabPage2x)
        Me.FlatTabControl1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatTabControl1.ItemSize = New System.Drawing.Size(120, 40)
        Me.FlatTabControl1.Location = New System.Drawing.Point(0, 35)
        Me.FlatTabControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatTabControl1.Name = "FlatTabControl1"
        Me.FlatTabControl1.SelectedIndex = 0
        Me.FlatTabControl1.Size = New System.Drawing.Size(1067, 420)
        Me.FlatTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.FlatTabControl1.TabIndex = 0
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage7.Controls.Add(Me.Panel6)
        Me.TabPage7.Controls.Add(Me.FlatLabel15)
        Me.TabPage7.Controls.Add(Me.ProgressBar2)
        Me.TabPage7.Controls.Add(Me.Panel4)
        Me.TabPage7.Controls.Add(Me.FlatButton3)
        Me.TabPage7.Controls.Add(Me.TextBox2)
        Me.TabPage7.Controls.Add(Me.TextBox1)
        Me.TabPage7.Controls.Add(Me.FlatButton1)
        Me.TabPage7.Controls.Add(Me.CheckedListBox1)
        Me.TabPage7.Controls.Add(Me.PictureBox5)
        Me.TabPage7.Location = New System.Drawing.Point(4, 44)
        Me.TabPage7.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage7.Size = New System.Drawing.Size(1059, 372)
        Me.TabPage7.TabIndex = 0
        Me.TabPage7.Text = "Modyfikacje"
        '
        'Panel6
        '
        Me.Panel6.Location = New System.Drawing.Point(349, 279)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(376, 27)
        Me.Panel6.TabIndex = 157
        '
        'FlatLabel15
        '
        Me.FlatLabel15.AutoSize = True
        Me.FlatLabel15.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel15.Font = New System.Drawing.Font("Segoe UI", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.FlatLabel15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatLabel15.Location = New System.Drawing.Point(462, 336)
        Me.FlatLabel15.Name = "FlatLabel15"
        Me.FlatLabel15.Size = New System.Drawing.Size(0, 23)
        Me.FlatLabel15.TabIndex = 156
        '
        'ProgressBar2
        '
        Me.ProgressBar2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.ProgressBar2.DarkerProgress = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ProgressBar2.ForeColor = System.Drawing.Color.Transparent
        Me.ProgressBar2.Location = New System.Drawing.Point(349, 281)
        Me.ProgressBar2.Maximum = 100
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.ProgressColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ProgressBar2.Size = New System.Drawing.Size(376, 42)
        Me.ProgressBar2.TabIndex = 155
        Me.ProgressBar2.Text = "FlatProgressBar1"
        Me.ProgressBar2.Value = 0
        '
        'Panel4
        '
        Me.Panel4.AllowDrop = True
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.Panel4.BackgroundImage = CType(resources.GetObject("Panel4.BackgroundImage"), System.Drawing.Image)
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Panel4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Panel4.Location = New System.Drawing.Point(573, 177)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(137, 98)
        Me.Panel4.TabIndex = 154
        Me.Panel4.Visible = False
        '
        'FlatButton3
        '
        Me.FlatButton3.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton3.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton3.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton3.Location = New System.Drawing.Point(585, 333)
        Me.FlatButton3.Name = "FlatButton3"
        Me.FlatButton3.Rounded = False
        Me.FlatButton3.Size = New System.Drawing.Size(140, 28)
        Me.FlatButton3.TabIndex = 153
        Me.FlatButton3.Text = "Dodaj paczkę"
        Me.FlatButton3.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(36, 360)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(10, 30)
        Me.TextBox2.TabIndex = 75
        Me.TextBox2.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(20, 360)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(10, 30)
        Me.TextBox1.TabIndex = 74
        Me.TextBox1.Visible = False
        '
        'FlatButton1
        '
        Me.FlatButton1.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatButton1.Location = New System.Drawing.Point(-1, 0)
        Me.FlatButton1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(210, 59)
        Me.FlatButton1.TabIndex = 20
        Me.FlatButton1.Text = "GRAJ"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.CheckedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CheckedListBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(734, 0)
        Me.CheckedListBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.ScrollAlwaysVisible = True
        Me.CheckedListBox1.Size = New System.Drawing.Size(326, 377)
        Me.CheckedListBox1.TabIndex = 1
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox5.Location = New System.Drawing.Point(731, -19)
        Me.PictureBox5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(5, 396)
        Me.PictureBox5.TabIndex = 21
        Me.PictureBox5.TabStop = False
        '
        'TabPage2x
        '
        Me.TabPage2x.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage2x.Controls.Add(Me.ListBox1)
        Me.TabPage2x.Controls.Add(Me.FlatButton4)
        Me.TabPage2x.Controls.Add(Me.FlatTextBox1)
        Me.TabPage2x.Controls.Add(Me.TextBox5x)
        Me.TabPage2x.Controls.Add(Me.TextBox4)
        Me.TabPage2x.Controls.Add(Me.modyx)
        Me.TabPage2x.Controls.Add(Me.TextBox2x)
        Me.TabPage2x.Controls.Add(Me.Button7x)
        Me.TabPage2x.Controls.Add(Me.Button4x)
        Me.TabPage2x.Controls.Add(Me.PictureBox8)
        Me.TabPage2x.Controls.Add(Me.Button9x)
        Me.TabPage2x.Controls.Add(Me.FlatLabel3)
        Me.TabPage2x.Location = New System.Drawing.Point(4, 44)
        Me.TabPage2x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage2x.Name = "TabPage2x"
        Me.TabPage2x.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TabPage2x.Size = New System.Drawing.Size(1059, 372)
        Me.TabPage2x.TabIndex = 5
        Me.TabPage2x.Text = "Multiplayer"
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.ListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 23
        Me.ListBox1.Location = New System.Drawing.Point(6, 4)
        Me.ListBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(506, 253)
        Me.ListBox1.TabIndex = 1
        '
        'FlatButton4
        '
        Me.FlatButton4.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton4.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton4.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton4.Location = New System.Drawing.Point(517, 4)
        Me.FlatButton4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatButton4.Name = "FlatButton4"
        Me.FlatButton4.Rounded = False
        Me.FlatButton4.Size = New System.Drawing.Size(95, 32)
        Me.FlatButton4.TabIndex = 35
        Me.FlatButton4.Text = "Dodaj"
        Me.FlatButton4.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatTextBox1
        '
        Me.FlatTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.FlatTextBox1.ForeColor = System.Drawing.Color.Silver
        Me.FlatTextBox1.Location = New System.Drawing.Point(343, 260)
        Me.FlatTextBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatTextBox1.Multiline = True
        Me.FlatTextBox1.Name = "FlatTextBox1"
        Me.FlatTextBox1.Size = New System.Drawing.Size(76, 33)
        Me.FlatTextBox1.TabIndex = 34
        Me.FlatTextBox1.Tag = ""
        Me.FlatTextBox1.Text = "Hasło"
        Me.FlatTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox5x
        '
        Me.TextBox5x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox5x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox5x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox5x.Location = New System.Drawing.Point(274, 261)
        Me.TextBox5x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox5x.Multiline = True
        Me.TextBox5x.Name = "TextBox5x"
        Me.TextBox5x.Size = New System.Drawing.Size(63, 33)
        Me.TextBox5x.TabIndex = 28
        Me.TextBox5x.Tag = ""
        Me.TextBox5x.Text = "Port"
        Me.TextBox5x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(1048, 42)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(377, 30)
        Me.TextBox4.TabIndex = 24
        Me.TextBox4.Visible = False
        '
        'modyx
        '
        Me.modyx.Location = New System.Drawing.Point(1048, 76)
        Me.modyx.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.modyx.Name = "modyx"
        Me.modyx.Size = New System.Drawing.Size(377, 30)
        Me.modyx.TabIndex = 23
        Me.modyx.Visible = False
        '
        'TextBox2x
        '
        Me.TextBox2x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox2x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox2x.Location = New System.Drawing.Point(97, 261)
        Me.TextBox2x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox2x.Multiline = True
        Me.TextBox2x.Name = "TextBox2x"
        Me.TextBox2x.Size = New System.Drawing.Size(167, 33)
        Me.TextBox2x.TabIndex = 11
        Me.TextBox2x.Tag = ""
        Me.TextBox2x.Text = "IP"
        Me.TextBox2x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button7x
        '
        Me.Button7x.BackColor = System.Drawing.Color.Transparent
        Me.Button7x.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Button7x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button7x.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Button7x.Location = New System.Drawing.Point(419, 261)
        Me.Button7x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button7x.Name = "Button7x"
        Me.Button7x.Rounded = False
        Me.Button7x.Size = New System.Drawing.Size(95, 32)
        Me.Button7x.TabIndex = 8
        Me.Button7x.Text = "Stwórz"
        Me.Button7x.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'Button4x
        '
        Me.Button4x.BackColor = System.Drawing.Color.Transparent
        Me.Button4x.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Button4x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4x.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Button4x.Location = New System.Drawing.Point(3, 262)
        Me.Button4x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button4x.Name = "Button4x"
        Me.Button4x.Rounded = False
        Me.Button4x.Size = New System.Drawing.Size(106, 32)
        Me.Button4x.TabIndex = 7
        Me.Button4x.Text = "Dołącz"
        Me.Button4x.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.PictureBox8.Location = New System.Drawing.Point(3, 2)
        Me.PictureBox8.Margin = New System.Windows.Forms.Padding(0)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(511, 257)
        Me.PictureBox8.TabIndex = 22
        Me.PictureBox8.TabStop = False
        '
        'Button9x
        '
        Me.Button9x.BackColor = System.Drawing.Color.Transparent
        Me.Button9x.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Button9x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button9x.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Button9x.Location = New System.Drawing.Point(518, 40)
        Me.Button9x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button9x.Name = "Button9x"
        Me.Button9x.Rounded = False
        Me.Button9x.Size = New System.Drawing.Size(94, 33)
        Me.Button9x.TabIndex = 10
        Me.Button9x.Text = "Usuń"
        Me.Button9x.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatLabel3
        '
        Me.FlatLabel3.AutoSize = True
        Me.FlatLabel3.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel3.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel3.ForeColor = System.Drawing.Color.White
        Me.FlatLabel3.Location = New System.Drawing.Point(258, 252)
        Me.FlatLabel3.Name = "FlatLabel3"
        Me.FlatLabel3.Size = New System.Drawing.Size(26, 41)
        Me.FlatLabel3.TabIndex = 13
        Me.FlatLabel3.Text = ":"
        '
        'FlatMini1
        '
        Me.FlatMini1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatMini1.BackColor = System.Drawing.Color.White
        Me.FlatMini1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatMini1.Font = New System.Drawing.Font("Marlett", 12.0!)
        Me.FlatMini1.Location = New System.Drawing.Point(1020, 2)
        Me.FlatMini1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatMini1.Name = "FlatMini1"
        Me.FlatMini1.Size = New System.Drawing.Size(18, 18)
        Me.FlatMini1.TabIndex = 2
        Me.FlatMini1.Text = "FlatMini1"
        Me.FlatMini1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.PictureBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox2.Location = New System.Drawing.Point(710, 4)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(248, 27)
        Me.PictureBox2.TabIndex = 5
        Me.PictureBox2.TabStop = False
        '
        'FlatClose1
        '
        Me.FlatClose1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatClose1.BackColor = System.Drawing.Color.White
        Me.FlatClose1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatClose1.Font = New System.Drawing.Font("Marlett", 10.0!)
        Me.FlatClose1.Location = New System.Drawing.Point(1045, 2)
        Me.FlatClose1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatClose1.Name = "FlatClose1"
        Me.FlatClose1.Size = New System.Drawing.Size(18, 18)
        Me.FlatClose1.TabIndex = 1
        Me.FlatClose1.Text = "FlatClose1"
        Me.FlatClose1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(640, 4)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(29, 27)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 21
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(675, 4)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(29, 27)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 22
        Me.PictureBox4.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(7, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(14, 31)
        Me.Panel1.TabIndex = 136
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(24, -2)
        Me.PictureBox9.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(200, 58)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox9.TabIndex = 24
        Me.PictureBox9.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.ClientSize = New System.Drawing.Size(1067, 455)
        Me.Controls.Add(Me.FormSkin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximumSize = New System.Drawing.Size(1067, 455)
        Me.MinimumSize = New System.Drawing.Size(1067, 455)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vlauncher"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.FormSkin1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlatTabControl1.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2x.ResumeLayout(False)
        Me.TabPage2x.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents FlatClose1 As Vlauncher.FlatClose
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents FlatMini1 As Vlauncher.FlatMini
    Friend WithEvents FlatTabControl1 As Vlauncher.FlatTabControl
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents FlatButton1 As Vlauncher.FlatButton
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage2x As System.Windows.Forms.TabPage
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents FlatButton4 As Vlauncher.FlatButton
    Friend WithEvents FlatTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5x As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents modyx As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2x As System.Windows.Forms.TextBox
    Friend WithEvents Button7x As Vlauncher.FlatButton
    Friend WithEvents Button4x As Vlauncher.FlatButton
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Button9x As Vlauncher.FlatButton
    Friend WithEvents FlatLabel3 As Vlauncher.FlatLabel
    Friend WithEvents PictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents FlatAlertBox1 As Vlauncher.FlatAlertBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents FlatLabel18 As Vlauncher.FlatLabel
    Friend WithEvents FlatButton6 As Vlauncher.FlatButton
    Friend WithEvents FlatColorPalette1 As Vlauncher.FlatColorPalette
    Friend WithEvents FlatButton5 As Vlauncher.FlatButton
    Friend WithEvents FlatComboBox1 As Vlauncher.FlatComboBox
    Friend WithEvents FlatLabel17 As Vlauncher.FlatLabel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents FlatTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10x As System.Windows.Forms.TextBox
    Friend WithEvents FlatLabel16 As Vlauncher.FlatLabel
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents FlatButton2 As Vlauncher.FlatButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CheckBox6x As Vlauncher.FlatToggle
    Friend WithEvents checkbox9x As Vlauncher.FlatToggle
    Friend WithEvents Checkbox3x As Vlauncher.FlatToggle
    Friend WithEvents CheckBox4x As Vlauncher.FlatToggle
    Friend WithEvents CheckBox1x As Vlauncher.FlatToggle
    Friend WithEvents FlatLabel14 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel13 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel12 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel11 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel10 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel9 As Vlauncher.FlatLabel
    Friend WithEvents checkbox5x As Vlauncher.FlatToggle
    Friend WithEvents checkbox7x As Vlauncher.FlatToggle
    Friend WithEvents checkbox10x As Vlauncher.FlatToggle
    Friend WithEvents CheckBox8x As Vlauncher.FlatToggle
    Friend WithEvents Checkbox12x As Vlauncher.FlatToggle
    Friend WithEvents CheckBox11x As Vlauncher.FlatToggle
    Friend WithEvents FlatLabel8 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel7 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel6 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel5 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel4 As Vlauncher.FlatLabel
    Friend WithEvents FlatLabel2 As Vlauncher.FlatLabel
    Friend WithEvents Checkbox13x As Vlauncher.FlatToggle
    Friend WithEvents FlatLabel1 As Vlauncher.FlatLabel
    Friend WithEvents checkbox14x As Vlauncher.FlatToggle
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ComboBox3x As Vlauncher.FlatComboBox
    Friend WithEvents ComboBox4 As Vlauncher.FlatComboBox
    Friend WithEvents ComboBox1x As Vlauncher.FlatComboBox
    Friend WithEvents ComboBox2x As Vlauncher.FlatComboBox
    Friend WithEvents FormSkin1 As Vlauncher.FormSkin
    Friend WithEvents FlatLabel19 As Vlauncher.FlatLabel
    Friend WithEvents autostartToggleButton As Vlauncher.FlatToggle
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents FlatLabel15 As Vlauncher.FlatLabel
    Friend WithEvents ProgressBar2 As Vlauncher.FlatProgressBar
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents FlatButton3 As Vlauncher.FlatButton

End Class
