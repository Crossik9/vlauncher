﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class profiladd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FormSkin1 = New Vlauncher.FormSkin()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FlatButton1 = New Vlauncher.FlatButton()
        Me.Button6x = New Vlauncher.FlatButton()
        Me.TextBox10x = New System.Windows.Forms.TextBox()
        Me.FormSkin1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FormSkin1
        '
        Me.FormSkin1.BackColor = System.Drawing.Color.White
        Me.FormSkin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.Controls.Add(Me.Panel1)
        Me.FormSkin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormSkin1.FlatColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FormSkin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.HeaderMaximize = False
        Me.FormSkin1.Location = New System.Drawing.Point(0, 0)
        Me.FormSkin1.Name = "FormSkin1"
        Me.FormSkin1.Size = New System.Drawing.Size(255, 91)
        Me.FormSkin1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(63, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.Panel1.Controls.Add(Me.FlatButton1)
        Me.Panel1.Controls.Add(Me.Button6x)
        Me.Panel1.Controls.Add(Me.TextBox10x)
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(246, 83)
        Me.Panel1.TabIndex = 0
        '
        'FlatButton1
        '
        Me.FlatButton1.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton1.Location = New System.Drawing.Point(6, 44)
        Me.FlatButton1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(100, 33)
        Me.FlatButton1.TabIndex = 144
        Me.FlatButton1.Text = "Anuluj"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'Button6x
        '
        Me.Button6x.BackColor = System.Drawing.Color.Transparent
        Me.Button6x.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Button6x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6x.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Button6x.Location = New System.Drawing.Point(136, 44)
        Me.Button6x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button6x.Name = "Button6x"
        Me.Button6x.Rounded = False
        Me.Button6x.Size = New System.Drawing.Size(100, 33)
        Me.Button6x.TabIndex = 143
        Me.Button6x.Text = "Dodaj"
        Me.Button6x.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'TextBox10x
        '
        Me.TextBox10x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox10x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox10x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox10x.Location = New System.Drawing.Point(8, 7)
        Me.TextBox10x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox10x.Multiline = True
        Me.TextBox10x.Name = "TextBox10x"
        Me.TextBox10x.Size = New System.Drawing.Size(228, 33)
        Me.TextBox10x.TabIndex = 142
        Me.TextBox10x.Tag = ""
        '
        'profiladd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(255, 91)
        Me.Controls.Add(Me.FormSkin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "profiladd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "profiladd"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.FormSkin1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FormSkin1 As Vlauncher.FormSkin
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextBox10x As System.Windows.Forms.TextBox
    Friend WithEvents FlatButton1 As Vlauncher.FlatButton
    Friend WithEvents Button6x As Vlauncher.FlatButton
End Class
