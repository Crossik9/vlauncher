﻿Imports System.Environment
Imports System.IO
Imports System.Net
Imports System.Diagnostics

Module armastartnormal

    Public Sub armastartnormal()
        Form1.TextBox4.Text = ""
        For Each ListItem As Integer In Form1.CheckedListBox1.CheckedIndices
            Form1.TextBox4.AppendText("-mod=" & Form1.CheckedListBox1.Items.Item(ListItem & " ") & " ")
        Next
        Form1.modyx.Text = ""
        If Form1.CheckBox5x.Checked = True Then Form1.modyx.AppendText("-window ")
        If Form1.CheckBox9x.Checked = True Then Form1.modyx.AppendText("-noFilePatching ")
        If Form1.CheckBox7x.Checked = True Then Form1.modyx.AppendText("-showScriptErrors ")
        If Form1.CheckBox6x.Checked = True Then Form1.modyx.AppendText("-nosplash ")
        If Form1.CheckBox8x.Checked = True Then Form1.modyx.AppendText("-noPause ")
        If Form1.CheckBox1x.Checked = True Then Form1.modyx.AppendText("-winxp ")
        If Form1.CheckBox10x.Checked = True Then Form1.modyx.AppendText("-maxMem" & Form1.ComboBox3x.SelectedItem & " ")
        If Form1.CheckBox4x.Checked = True Then Form1.modyx.AppendText("-cpuCount=" & Form1.ComboBox2x.SelectedItem & " ")
        If Form1.CheckBox3x.Checked = True Then Form1.modyx.AppendText("-exThreads=" & Form1.ComboBox1x.SelectedItem & " ")
        If Form1.CheckBox12x.Checked = True Then Form1.modyx.AppendText(Form1.TextBox10x.Text)
        If Form1.CheckBox11x.Checked = True Then

            If Form1.ComboBox4.SelectedItem = "Zamknij" Then
                Form1.Close()
            End If
            If Form1.ComboBox4.SelectedItem = "Zminimalizuj" Then
                Form1.WindowState = FormWindowState.Minimized
            End If
        End If
    End Sub
End Module
