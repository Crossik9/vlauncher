﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addserwer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FormSkin1 = New Vlauncher.FormSkin()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.FlatButton1 = New Vlauncher.FlatButton()
        Me.TextBox7x = New System.Windows.Forms.TextBox()
        Me.TextBox8x = New System.Windows.Forms.TextBox()
        Me.Button6x = New Vlauncher.FlatButton()
        Me.FormSkin1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FormSkin1
        '
        Me.FormSkin1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.Controls.Add(Me.Panel1)
        Me.FormSkin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FormSkin1.FlatColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FormSkin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FormSkin1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FormSkin1.HeaderMaximize = False
        Me.FormSkin1.Location = New System.Drawing.Point(0, 0)
        Me.FormSkin1.Name = "FormSkin1"
        Me.FormSkin1.Size = New System.Drawing.Size(490, 94)
        Me.FormSkin1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.FlatButton1)
        Me.Panel1.Controls.Add(Me.TextBox7x)
        Me.Panel1.Controls.Add(Me.TextBox8x)
        Me.Panel1.Controls.Add(Me.Button6x)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(484, 88)
        Me.Panel1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(7, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(185, 28)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "zablokować znak "":"""
        '
        'FlatButton1
        '
        Me.FlatButton1.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton1.Location = New System.Drawing.Point(248, 48)
        Me.FlatButton1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(100, 33)
        Me.FlatButton1.TabIndex = 37
        Me.FlatButton1.Text = "Anuluj"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'TextBox7x
        '
        Me.TextBox7x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox7x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox7x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox7x.Location = New System.Drawing.Point(248, 11)
        Me.TextBox7x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox7x.Multiline = True
        Me.TextBox7x.Name = "TextBox7x"
        Me.TextBox7x.Size = New System.Drawing.Size(230, 33)
        Me.TextBox7x.TabIndex = 36
        Me.TextBox7x.Tag = ""
        Me.TextBox7x.Text = "IP"
        Me.TextBox7x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox8x
        '
        Me.TextBox8x.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.TextBox8x.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox8x.ForeColor = System.Drawing.Color.Silver
        Me.TextBox8x.Location = New System.Drawing.Point(12, 11)
        Me.TextBox8x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextBox8x.Multiline = True
        Me.TextBox8x.Name = "TextBox8x"
        Me.TextBox8x.Size = New System.Drawing.Size(230, 33)
        Me.TextBox8x.TabIndex = 35
        Me.TextBox8x.Tag = ""
        Me.TextBox8x.Text = "Nazwa"
        Me.TextBox8x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button6x
        '
        Me.Button6x.BackColor = System.Drawing.Color.Transparent
        Me.Button6x.BaseColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(96, Byte), Integer))
        Me.Button6x.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6x.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Button6x.Location = New System.Drawing.Point(378, 48)
        Me.Button6x.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button6x.Name = "Button6x"
        Me.Button6x.Rounded = False
        Me.Button6x.Size = New System.Drawing.Size(100, 33)
        Me.Button6x.TabIndex = 34
        Me.Button6x.Text = "Dodaj"
        Me.Button6x.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'addserwer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(490, 94)
        Me.Controls.Add(Me.FormSkin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "addserwer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "addserwer"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.FormSkin1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FormSkin1 As Vlauncher.FormSkin
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlatButton1 As Vlauncher.FlatButton
    Friend WithEvents TextBox7x As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8x As System.Windows.Forms.TextBox
    Friend WithEvents Button6x As Vlauncher.FlatButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
