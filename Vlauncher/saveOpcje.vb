﻿Imports System
Imports System.IO
Imports System.Text
Module saveOpcje
    Public INI_File As New IniFile(My.Application.Info.DirectoryPath + "/opcje.ini")
    Sub wczytywanie()
        Dim INI_File As New IniFile(My.Application.Info.DirectoryPath + "/opcje.ini")

        For Each ListItem As Integer In Form1.CheckedListBox1.CheckedIndices
            Form1.TextBox4.AppendText("-mod=" & Form1.CheckedListBox1.Items.Item(ListItem & " "))
        Next
        If (INI_File.GetString("Opcje", "checkbox13x", "(none)")) = 0 Then
            Form1.Checkbox13x.Checked = False
        Else
            Form1.Checkbox13x.Checked = True
        End If
        If (INI_File.GetString("Opcje", "checkbox14x", "(none)")) = 0 Then
            Form1.checkbox14x.Checked = False
        Else
            Form1.checkbox14x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox11x", "(none)")) = 0 Then
            Form1.CheckBox11x.Checked = False
        Else
            Form1.CheckBox11x.Checked = True
        End If
        If (INI_File.GetString("Opcje", "checkbox12x", "(none)")) = 0 Then
            Form1.Checkbox12x.Checked = False
        Else
            Form1.Checkbox12x.Checked = True
        End If
        If (INI_File.GetString("Opcje", "checkbox9x", "(none)")) = 0 Then
            Form1.checkbox9x.Checked = False
        Else
            Form1.checkbox9x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox7x", "(none)")) = 0 Then
            Form1.checkbox7x.Checked = False
        Else
            Form1.checkbox7x.Checked = True
        End If


        If (INI_File.GetString("Opcje", "checkbox5x", "(none)")) = 0 Then
            Form1.checkbox5x.Checked = False
        Else
            Form1.checkbox5x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox6x", "(none)")) = 0 Then
            Form1.CheckBox6x.Checked = False
        Else
            Form1.CheckBox6x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox8x", "(none)")) = 0 Then
            Form1.CheckBox8x.Checked = False
        Else
            Form1.CheckBox8x.Checked = True
        End If
        If (INI_File.GetString("Opcje", "autostart", "(none)")) = 0 Then
            Form1.autostartToggleButton.Checked = False
        Else
            Form1.autostartToggleButton.Checked = True
        End If
        If (INI_File.GetString("Opcje", "checkbox1x", "(none)")) = 0 Then
            Form1.CheckBox1x.Checked = False
        Else
            Form1.CheckBox1x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox4x", "(none)")) = 0 Then
            Form1.CheckBox4x.Checked = False
        Else
            Form1.CheckBox4x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox10x", "(none)")) = 0 Then
            Form1.checkbox10x.Checked = False
        Else
            Form1.checkbox10x.Checked = True
        End If

        If (INI_File.GetString("Opcje", "checkbox3x", "(none)")) = 0 Then
            Form1.Checkbox3x.Checked = False
        Else
            Form1.Checkbox3x.Checked = True
        End If


        Form1.ComboBox1x.SelectedItem = (INI_File.GetString("Opcje", "combo1", "(none)"))
        Form1.ComboBox2x.SelectedItem = (INI_File.GetString("Opcje", "combo2", "(none)"))
        Form1.ComboBox3x.SelectedItem = (INI_File.GetString("Opcje", "combo3", "(none)"))
        Form1.ComboBox4.SelectedItem = (INI_File.GetString("Opcje", "combo4", "(none)"))

        'tutaj wczytywanie profili do flatcomboboxa1

        Dim modyE As String = INI_File.GetString("Opcje", "wszystkieprofile", "(none)")
        If modyE = "" Then
        Else
            Dim FD As Integer = modyE.Split("~").Length - 1
            For a = 1 To FD
                Dim ip2x() As String
                ip2x = modyE.Split("~")
                Form1.FlatComboBox1.Items.Add(ip2x(FX))
                FX += 1
            Next
            FX = 0
        End If
        '        
        Form1.FlatComboBox1.SelectedItem = (INI_File.GetString("Opcje", "combo5", "(none)"))

        Form1.TextBox10x.Text = (INI_File.GetString("Opcje", "textbox10x", "(none)"))
        Form1.FlatTextBox2.Text = (INI_File.GetString("Opcje", "sciezka", "(none)"))

        '
        Form1.modyx.Text = ""
        If Form1.checkbox5x.Checked = True Then Form1.modyx.AppendText("-window ")
        If Form1.checkbox9x.Checked = True Then Form1.modyx.AppendText("-noFilePatching ")
        If Form1.checkbox7x.Checked = True Then Form1.modyx.AppendText("-showScriptErrors ")
        If Form1.CheckBox6x.Checked = True Then Form1.modyx.AppendText("-nosplash ")
        If Form1.CheckBox8x.Checked = True Then Form1.modyx.AppendText("-noPause ")
        If Form1.CheckBox1x.Checked = True Then Form1.modyx.AppendText("-winxp ")
        If Form1.checkbox10x.Checked = True Then Form1.modyx.AppendText("-maxMem" & Form1.ComboBox3x.SelectedItem & " ")
        If Form1.CheckBox4x.Checked = True Then Form1.modyx.AppendText("-cpuCount=" & Form1.ComboBox2x.SelectedItem & " ")
        If Form1.Checkbox3x.Checked = True Then Form1.modyx.AppendText("-exThreads=" & Form1.ComboBox1x.SelectedItem & " ")
        For Each foundFile As String In My.Computer.FileSystem.GetDirectories(My.Application.Info.DirectoryPath, FileIO.SearchOption.SearchAllSubDirectories, "@*")
            Dim index As Integer = foundFile.IndexOf("@")
            Dim output As String = foundFile.Substring(index, foundFile.Length - index)
            Form1.CheckedListBox1.Items.Add(output)
        Next
        Dim FILE_NAME As String = My.Application.Info.DirectoryPath & "\mody.txt"
        Dim objReader As New System.IO.StreamReader(FILE_NAME)

        If System.IO.File.Exists(FILE_NAME) = True Then
            Do While objReader.Peek() <> -1
                Form1.CheckedListBox1.SetItemChecked((objReader.ReadLine()), True)
            Loop
        End If
        objReader.Close()
        '




        If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\opcje.ini") Then

            Dim x As Integer = INI_File.GetString("index", "najwiekszy", "(none)")
            Dim y As Integer = 1
            For a = 1 To x
                Dim check As String = INI_File.GetString("Serwery", y, "(none)")
                y += 1

                If check = "(none)" Then
                Else
                    Form1.ListBox1.Items.Add(check)
                End If
            Next

        Else
            MsgBox("Error 153 - brak pliku konfiguracyjnego. Ponowna instalacja programu może naprawić problem")
        End If


        If My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "\mody.txt") Then
        Else : File.Create(My.Application.Info.DirectoryPath & "\mody.txt")
        End If
        wczytywaniemodow()

        ' '' '' '' ''If Form1.autostartToggleButton.Checked = True Then
        ' '' '' '' ''    If My.Computer.FileSystem.FileExists(My.Computer.FileSystem.SpecialDirectories.Programs + "\startup\Vlauncher.exe") Then
        ' '' '' '' ''    Else
        ' '' '' '' ''        FileCopy("Vlauncher.exe", My.Computer.FileSystem.SpecialDirectories.Programs + "\startup\Vlauncher.exe")
        ' '' '' '' ''    End If


        ' '' '' '' ''Else

        ' '' '' '' ''End If


    End Sub

    Public Sub wczytywaniemodow()
        For i As Integer = 0 To Form1.CheckedListBox1.Items.Count - 1
            Form1.CheckedListBox1.SetItemChecked(i, False)
        Next
        Dim mody As String = INI_File.GetString("Profile", Form1.FlatComboBox1.SelectedItem, "(none)")
        If mody = "" Then
        Else
            Dim DX As Integer = mody.Split("@").Length - 1

            For a = 1 To DX
                Dim indexmoda As String
                Dim ip2() As String
                ip2 = mody.Split("~")
                indexmoda = Form1.CheckedListBox1.Items.IndexOf(ip2(xD))
                Form1.CheckedListBox1.SetItemChecked(indexmoda, True)
                xD += 1
            Next
            xD = 0
        End If
    End Sub

    Public Sub saveopcje()

        Dim INI_File As New IniFile(My.Application.Info.DirectoryPath + "/opcje.ini")
        For Each items As Object In Form1.FlatComboBox1.Items
            Form1.TextBox2.AppendText(items.ToString + "~")
        Next
        INI_File.WriteString("Opcje", "wszystkieprofile", Form1.TextBox2.Text)
        Form1.TextBox2.Clear()
        If Form1.checkbox14x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox14x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox14x", "0")
        End If

        INI_File.WriteString("Opcje", "combo5", Form1.FlatComboBox1.SelectedItem)


        If Form1.Checkbox13x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox13x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox13x", "0")
        End If
        If Form1.CheckBox11x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox11x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox11x", "0")
        End If

        If Form1.Checkbox12x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox12x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox12x", "0")
        End If
        If Form1.CheckBox1x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox1x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox1x", "0")
        End If
        If Form1.checkbox7x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox7x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox7x", "0")
        End If
        If Form1.autostartToggleButton.Checked = True Then
            INI_File.WriteString("Opcje", "autostart", "1")
        Else
            INI_File.WriteString("Opcje", "autostart", "0")
        End If
        If Form1.checkbox5x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox5x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox5x", "0")
        End If
        If Form1.CheckBox6x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox6x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox6x", "0")
        End If
        If Form1.CheckBox8x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox8x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox8x", "0")
        End If
        If Form1.checkbox10x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox10x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox10x", "0")
        End If
        If Form1.Checkbox3x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox3x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox3x", "0")
        End If
        If Form1.CheckBox4x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox4x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox4x", "0")
        End If
        If Form1.checkbox9x.Checked = True Then
            INI_File.WriteString("Opcje", "checkbox9x", "1")
        Else
            INI_File.WriteString("Opcje", "checkbox9x", "0")
        End If

        INI_File.WriteString("Opcje", "combo1", Form1.ComboBox1x.SelectedItem)
        INI_File.WriteString("Opcje", "combo2", Form1.ComboBox2x.SelectedItem)
        INI_File.WriteString("Opcje", "combo3", Form1.ComboBox3x.SelectedItem)
        INI_File.WriteString("Opcje", "combo4", Form1.ComboBox4.SelectedItem)
        INI_File.WriteString("Opcje", "textbox10x", Form1.TextBox10x.Text)
        INI_File.WriteString("Opcje", "sciezka", Form1.FlatTextBox2.Text)

        For Each items As Object In Form1.CheckedListBox1.CheckedItems
            Form1.TextBox1.AppendText(items.ToString + "~")
        Next
        INI_File.WriteString("Profile", Form1.FlatComboBox1.SelectedItem, Form1.TextBox1.Text)
        Form1.TextBox1.Clear()
    End Sub
    Public xD As Integer = 0
    Public FX As Integer = 0
End Module
