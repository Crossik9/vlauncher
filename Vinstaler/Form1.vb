﻿Imports System.IO.FileInfo
Public Class Form1
    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) Handles FlatButton1.Click
        FolderBrowserDialog1.ShowDialog()
        FlatTextBox1.Text = FolderBrowserDialog1.SelectedPath.ToString
        Timer1.Enabled = True
        Timer1.Start()
        If My.Computer.FileSystem.FileExists(FlatTextBox1.Text + "\ArmA2OA.exe") Then
            Timer4.Enabled = True
            Timer4.Start()
        Else
            Timer5.Start()
            Timer5.Start()
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        FlatLabel7.Visible = False
        FlatLabel5.Visible = True
        Timer2.Enabled = True
        Timer2.Start()
        Timer1.Stop()
        Timer1.Enabled = False
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        FlatLabel5.Visible = False
        FlatLabel6.Visible = True
        Timer3.Enabled = True
        Timer3.Start()
        Timer2.Stop()
        Timer2.Enabled = False
    End Sub
    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        FlatLabel6.Visible = False
        FlatLabel7.Visible = True
        Timer1.Enabled = True
        Timer1.Start()
        Timer3.Stop()
        Timer3.Enabled = False
    End Sub
    Private Sub Timer4_Tick(sender As Object, e As EventArgs) Handles Timer4.Tick
        Timer1.Enabled = False
        Timer1.Stop()
        Timer2.Enabled = False
        Timer2.Stop()
        Timer3.Enabled = False
        Timer3.Stop()
        FlatLabel5.Visible = True
        FlatLabel6.Visible = True
        FlatLabel7.Visible = True
        FlatLabel5.ForeColor = FlatColorPalette1.LimeGreen
        FlatLabel6.ForeColor = FlatColorPalette1.LimeGreen
        FlatLabel7.ForeColor = FlatColorPalette1.LimeGreen
        FlatLabel3.Visible = False
        FlatCheckBox1.Visible = True
        LinkLabel1.Visible = True
        FlatLabel9.Visible = True
        FlatLabel10.Visible = True
        FlatComboBox1.Visible = True
        FlatButton2.Visible = True
        Panel2.Visible = True
        FlatToggle1.Visible = True
        Timer4.Stop()
        Timer4.Enabled = False
    End Sub
    Private Sub Timer5_Tick(sender As Object, e As EventArgs) Handles Timer5.Tick
        Timer1.Enabled = False
        Timer1.Stop()
        Timer2.Enabled = False
        Timer2.Stop()
        Timer3.Enabled = False
        Timer3.Stop()
        FlatLabel5.Visible = True
        FlatLabel6.Visible = True
        FlatLabel7.Visible = True
        FlatLabel5.ForeColor = FlatColorPalette1.Red
        FlatLabel6.ForeColor = FlatColorPalette1.Red
        FlatLabel7.ForeColor = FlatColorPalette1.Red
        FlatLabel3.Visible = True

        Timer5.Stop()
        Timer5.Enabled = False
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object) Handles RadioButton1.CheckedChanged
        FlatLabel2.Visible = True
        FlatLabel5.Visible = True
        FlatLabel6.Visible = True
        FlatLabel7.Visible = True
        FlatTextBox1.Visible = True
        FlatButton1.Visible = True
        Me.Refresh()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object) Handles RadioButton2.CheckedChanged
        FlatLabel2.Visible = True
        FlatLabel5.Visible = True
        FlatLabel6.Visible = True
        FlatLabel7.Visible = True
        FlatTextBox1.Visible = True
        FlatButton1.Visible = True
        Me.Refresh()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        If FlatComboBox1.SelectedIndex < 0 Then FlatComboBox1.SelectedIndex = 0
        FlatLabel2.Visible = False
        FlatLabel5.Visible = False
        FlatLabel6.Visible = False
        FlatLabel7.Visible = False
        FlatTextBox1.Visible = False
        FlatButton1.Visible = False
        FlatCheckBox1.Visible = False
        LinkLabel1.Visible = False
        FlatLabel9.Visible = False
        FlatLabel10.Visible = False
        FlatComboBox1.Visible = False
        FlatToggle1.Visible = False
        Panel2.Visible = False
        Me.Refresh()
    End Sub

    Private Sub FlatButton2_Click(sender As Object, e As EventArgs) Handles FlatButton2.Click
        If RadioButton1.Checked = True Then
            RichTextBox2.Text = ("jezyk=ENG" & vbNewLine)
        End If

        If RadioButton2.Checked = True Then
            RichTextBox2.Text = ("jezyk=PL" & vbNewLine)
        End If
        If FlatToggle1.Checked = True Then
            RichTextBox1.Text = ("autostart=1" & vbNewLine)
        Else
            RichTextBox1.Text = ("autostart=0" & vbNewLine)
        End If
        Panel3.Show()
        'instalacja rozpoczęta


        Label8.Text = Label8.Text + "-Instalacja rozpoczęta." + vbNewLine

        System.IO.File.Create(FlatTextBox1.Text + "\mody.txt")
        Label8.Text = Label8.Text + "-Plik 'mody.txt' stworzony" + vbNewLine

        Dim objw As New System.IO.StreamWriter(FlatTextBox1.Text + "\opcje.ini")
        objw.Write("[Opcje]" & vbNewLine & "checkbox1x=0" & vbNewLine & "checkbox7x=0" & vbNewLine & "checkbox5x=0" & vbNewLine & "checkbox6x=0" & vbNewLine & "checkbox8x=0" & vbNewLine & "checkbox10x=0" & vbNewLine & "checkbox4x=0" & vbNewLine & "checkbox3x=0" & vbNewLine & "checkbox9x=0" & vbNewLine & "checkbox11x=0" & vbNewLine & "checkbox12x=0" & vbNewLine & "checkbox13x=1" & vbNewLine & "checkbox14x=1" & vbNewLine & "combo1=Niski" & vbNewLine & "combo2=1" & vbNewLine & "combo3=2047" & vbNewLine & "combo4 = Zamknij" & vbNewLine & "textbox10x=" & vbNewLine & RichTextBox1.Text & vbNewLine & RichTextBox2.Text & vbNewLine & "sciezka=" & FlatTextBox1.Text & vbNewLine & "kolor=" & FlatComboBox1.SelectedItem.ToString & vbNewLine & "[index]" & vbNewLine & "najwiekszy=1" & vbNewLine & "[Serwery]" & vbNewLine & "1=Veterani~87.204.29.66:2302" & vbNewLine)
        objw.Close()
        Label8.Text = Label8.Text + "-Plik 'opcje.ini' stworzony." + vbNewLine

        Label8.Text = Label8.Text + "-Małe poprawki..." + vbNewLine


        Label8.Text = Label8.Text + "-Formatowanie dys.. tylo żaruje :P" + vbNewLine


        Label8.Text = Label8.Text + "-Gotowe!" + vbNewLine
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        MsgBox("I tak byś jej nie czytał..")
    End Sub

    Private Sub FlatCheckBox1_CheckedChanged(sender As Object) Handles FlatCheckBox1.CheckedChanged
        If FlatCheckBox1.Checked = True Then
            FlatButton2.Enabled = True
            FlatButton2.BaseColor = FlatColorPalette1.LimeGreen
            FlatButton2.Refresh()
        Else
            FlatButton2.Enabled = False
            FlatButton2.BaseColor = Color.Gray
            FlatButton2.Refresh()
        End If

    End Sub
    Private Sub FlatComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FlatComboBox1.SelectedIndexChanged
        If FlatComboBox1.SelectedItem = "Orange" Then
            Panel2.BackColor = FlatColorPalette1.Orange
        End If
        If FlatComboBox1.SelectedItem = "Red" Then
            Panel2.BackColor = FlatColorPalette1.Red
        End If
        If FlatComboBox1.SelectedItem = "Green" Then
            Panel2.BackColor = FlatColorPalette1.LimeGreen
        End If
        If FlatComboBox1.SelectedItem = "Blue" Then
            Panel2.BackColor = FlatColorPalette1.Blue
        End If
        If FlatComboBox1.SelectedItem = "Cyan" Then
            Panel2.BackColor = FlatColorPalette1.Cyan
        End If
        If FlatComboBox1.SelectedItem = "Purple" Then
            Panel2.BackColor = FlatColorPalette1.Purple
        End If
    End Sub

    Private Sub FlatClose1_Click(sender As Object, e As EventArgs) Handles FlatClose1.Click

    End Sub
End Class
